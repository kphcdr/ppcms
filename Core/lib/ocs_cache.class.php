<?php
class ocs_cache
{
    private $cache;
    
    /** 
     * Memcache缓存-设置链接服务器 
     * @param  array $servers
     */
    public function add_server($servers = array('127.0.0.1', '11211'),$auth = null)
    {
        $this->cache = new Memcached();
		$this->cache->addServer($servers[0],$servers[1]);
		$this->cache->setOption(Memcached::OPT_COMPRESSION, false); //关闭压缩功能
		$this->cache->setOption(Memcached::OPT_BINARY_PROTOCOL, true); //使用binary二进制协议
		if($auth)
		{
			$this->cache->setSaslAuthData($auth[0],$auth[1]);
		}
    }
    /** 
     * Memcache缓存-设置缓存 
     * 设置缓存key，value和缓存时间 
     * @param  string $key   KEY值 
     * @param  string $value 值 
     * @param  string $time  缓存时间 
     */
    public function set($key, $value, $time = 0)
    {
        return $this->cache->set($key, $value, $time);
    }
    /** 
     * Memcache缓存-获取缓存 
     * 通过KEY获取缓存数据 
     * @param  string $key   KEY值 
     */
    public function get($key)
    {
        return $this->cache->get($key);
    }
    /** 
     * Memcache缓存-清除一个缓存 
     * 从cache中删除一条缓存 
     * @param  string $key   KEY值 
     */
    public function del($key)
    {
        return $this->cache->delete($key);
    }
    /** 
     * Memcache缓存-清空所有缓存 
     * 不建议使用该功能 
      */
    public function clear()
    {
        return $this->cache->flush();
    }
    /** 
     * 字段自增-用于记数 
     * @param string $key  KEY值 
     * @param int    $step 新增的step值 
     */
    public function increment($key, $step = 1)
    {
        return $this->cache->increment($key, (int)$step);
    }
    /** 
     * 字段自减-用于记数 
     * @param string $key  KEY值 
     * @param int    $step 新增的step值 
     */
    public function decrement($key, $step = 1)
    {
        return $this->cache->decrement($key, (int) $step);
    }
    /** 
     * 关闭Memcache链接 
     */
    public function close()
    {
        return $this->cache->close();
    }
    /** 
     * 替换数据 
     * @param string $key KEY值 
     * @param string $value 替换后的值 
     * @param int    $time  时间值 
     * @param bool   $flag  是否进行压缩 
     */
    public function replace($key, $value, $time = 0, $flag = false)
    {
        return $this->cache->replace($key, $value, false, $time);
    }
    /** 
     * 获取Memcache的版本号 
     */
    public function getVersion()
    {
        return $this->cache->getVersion();
    }
    /** 
     * 获取Memcache的状态数据 
     */
    public function getStats()
    {
        return $this->cache->getStats();
    }
}