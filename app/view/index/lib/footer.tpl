		<div class="tm-footer">
			<div class="uk-container uk-container-center uk-text-center">
				<div class="uk-panel">友情链接
				<ul class="uk-subnav uk-subnav-line">
				{foreach from=$link item=a}
				<li><a href="{$a.url}">{$a.name}</a></li>
				{/foreach}
				</ul>
					<p>
						POWER BY
						<a href="http://www.kphcdr.com">
							kphcdr.com
						</a>
						THEME BY
						<a href="www.getuikit.net">
							UIKIT
						</a>
						<br />
						<a href="###">
						 ICP备案号：{$config.icp}
						</a>
					</p>
				</div>
			</div>
		</div>
		<div id="tm-offcanvas" class="uk-offcanvas">
			<div class="uk-offcanvas-bar">
				<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
					<li class="uk-parent uk-active">
						<a href="#">
							最近文章
						</a>
						<ul class="uk-nav-sub">
							{foreach from=$news.data item=a}
							<li>
								<a href="{'index/news/'|url}id/{$a.id}">
									{$a.name}
								</a>
							</li>
							{/foreach}
						</ul>
					</li>
					<li class="uk-parent uk-active">
					<a href="###">
								栏目导航
					</a>		
						<ul class="uk-nav-sub">		
					{foreach from=$category item=a}
					<li>
						<a href="{'index/c/'|url}id/{$a.id}">
							{$a.name}
						</a>
					</li>
						</ul>
					{/foreach}
					</li>
				</ul>
			</div>
		</div>
		<div style="display:none"><script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_5839432'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s5.cnzz.com/stat.php%3Fid%3D5839432' type='text/javascript'%3E%3C/script%3E"));</script></div>