		<nav class="tm-navbar uk-navbar uk-navbar-attached">
			<div class="uk-container uk-container-center">
				<a class="uk-navbar-brand uk-hidden-small" href="###">
					PPCMS
				</a>
				<ul class="uk-navbar-nav uk-hidden-small">
					<li {if $id eq 0}class="uk-active"{/if}>
						<a href="/">
							首页
						</a>
					</li>
					{foreach from=$category item=a}
					<li {if $id eq $a.id}class="uk-active"{/if}>
						<a href="{'index/c/'|url}id/{$a.id}">{$a.name}</a>
					</li>
					{/foreach}
				</ul>
				<a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas>
				</a>
				<div class="uk-navbar-brand uk-navbar-center uk-visible-small">
					PPCMS
				</div>
			</div>
		</nav>