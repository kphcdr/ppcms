<!DOCTYPE html>
<html lang="zh-cn" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{$config.name}</title>
		<meta name="description" content="{$config.desc}" />
		<meta name="keywords" content="{$config.keyword}" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">
		<link id="data-uikit-theme" rel="stylesheet" href="http://www.getuikit.net/docs/css/uikit.docs.min.css">
		<link rel="stylesheet" href="http://kphcdr.oss-cn-qingdao.aliyuncs.com/static/index/css/docs.css">
		<link rel="stylesheet" href="http://kphcdr.oss-cn-qingdao.aliyuncs.com/static/index/js/highlight/highlight.css">
		<script src="http://kphcdr.oss-cn-qingdao.aliyuncs.com/static/index/js/jquery.js"></script>
		<script src="http://kphcdr.oss-cn-qingdao.aliyuncs.com/static/index/js/uikit.min.js"></script>
		<script src="http://kphcdr.oss-cn-qingdao.aliyuncs.com/static/index/js/highlight/highlight.js"></script>
	</head>
	<body class="tm-background">
{include file="index/lib/header.tpl"}
		<div class="tm-middle">
			<div class="uk-container uk-container-center">
				<div class="uk-grid" data-uk-grid-margin>
					<div class="tm-sidebar uk-width-medium-1-4 uk-hidden-small">
						<ul class="tm-nav uk-nav" data-uk-nav>
							<li class="uk-nav-header">
								栏目导航
							</li>
					{foreach from=$category item=a}
					<li>
						<a href="{'index/c/'|url}id/{$a.id}">
							{$a.name}
						</a>
					</li>
					{/foreach}
					<li class="uk-nav-header">
						部分文章
					</li>
					{foreach from=$news.data item=a}
						<li>
							<a href="{'index/news/'|url}id/{$a.id}" title="{$a.name}">
								{$a.name|truncate:25}
							</a>
						</li>
					{/foreach}					
						</ul>
					</div>
					<div class="tm-main uk-width-medium-3-4">
						<article class="uk-article">
							{foreach from=$news['data'] name=a item=a}
							<h3 class="tm-article-subtitle">
								<a href="{'index/news/'|url}id/{$a.id}">{$a.name}</a>
							</h3>
							{$a.desc} <a href="{'index/news/'|url}id/{$a.id}">阅读全文</a>
							{/foreach}
						</article>
						
						<ul class="uk-pagination">{$news.page}</ul>
					</div>
					
				</div>
			</div>
		</div>
{include file="index/lib/footer.tpl"}
	</body>

</html>