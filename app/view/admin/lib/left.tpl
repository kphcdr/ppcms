    <div class="pure-u">
        <div class="side">
            <div class="pure-menu pure-menu-open side-menu">
                <ul>
                    <li class="pure-menu-heading side-menu-head"><i class="icon-tags"></i>文章相关</li>
                    <li {if $__a eq 'lists'}class="pure-menu-selected"{/if}><a href="/admin/lists" >文章列表</a></li>
                    <li {if $__a eq 'category'}class="pure-menu-selected"{/if}><a href="/admin/category">文章分类</a></li>                  
                    <li {if $__a eq 'article'}class="pure-menu-selected"{/if}><a href="/admin/article">文章单页</a></li>  
                    <li class="pure-menu-heading side-menu-head"><i class="icon-tags"></i>会员行为</li>
                    <li {if $__a eq 'member'}class="pure-menu-selected"{/if}><a href="/admin/member">会员列表</a></li>  
                    <li {if $__a eq 'guestbook'}class="pure-menu-selected"{/if}><a href="/admin/guestbook">留言</a></li>  
					
                    <li class="pure-menu-heading side-menu-head"><i class="icon-tags"></i>系统设置</li>
                    <li {if $__a eq 'config'}class="pure-menu-selected"{/if}><a href="/admin/config">系统设置</a></li>
                    <li {if $__a eq 'link'}class="pure-menu-selected"{/if}><a href="/admin/link">友情链接</a></li>
                </ul>
            </div>
        </div>
    </div>