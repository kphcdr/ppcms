<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>文章列表</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/static/admin/css/pure-nr-min.css">
<link rel="stylesheet" href="/static/admin/css/fontAwesome/font-awesome.css">
<link rel="stylesheet" href="/static/admin/css/main.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">      
</head>
<body>
<!--BEGIN HEADER-->
{include file="admin/lib/header.tpl"}
<!--END HEADER-->
<div class="pure-g content">
<!--BEGIN left-->
{include file="admin/lib/left.tpl"}
<!--END left-->
    <div class="pure-u-1">
        <div class="main">
            <div class="panel panel-default" >
                <div class="panel-title">
                    文章列表<a class="pure-button pure-button-primary fr" style="float:right" href="{'admin/addnews'|url}"><i class="icon-plus-square"></i>添加文章</a>
                </div>
				<div class="panel-body">
<table class="pure-table" width="100%" >
    <thead>
        <tr>
            <th width="5%">ID</th>
            <th width="35%">名称</th>
            <th width="15">是否可用</th>
            <th width="10%">所属分类</th>
            <th width="20%">发布时间</th>
            <th width="15%">操作</th>
        </tr>
    </thead>
    <tbody>
	{foreach from=$ca item=a}
	    <tr>
            <td>{$a.id}</td>
            <td>{$a.name}</td>
            <td>{if $a.is_use eq 1}是{else}否{/if}</td>			
            <td>{$a.cn}</td>
            <td>{$a.createtime|date_format:"%Y-%m-%d %H:%M:%S"}</td>
			<td><a class="pure-button pure-button-xsmall" href="{'admin/addnews/id/'|url}{$a.id}"><i class="icon-edit"></i></a>
            <a class="pure-button pure-button-xsmall" href="{'admin/delnews/id/'|url}{$a.id}"><i class="icon-trash"></i></a></td>
        </tr>
	{/foreach}
    </tbody>
</table>

				<ul class="pure-paginator pure-menu">
{$page}
				</ul>

				</div>
			</div>		
        </div>
    </div>
</div>
<!--BEGIN FOOT-->
{include file="admin/lib/copyright.tpl"}
<!--END FOOT-->
</body>
</html>