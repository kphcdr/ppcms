<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>友情链接</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/static/admin/css/pure-nr-min.css">
<link rel="stylesheet" href="/static/admin/css/fontAwesome/font-awesome.css">
<link rel="stylesheet" href="/static/admin/css/main.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">   

</head>
<body>
<!--BEGIN HEADER-->
{include file="admin/lib/header.tpl"}
<!--END HEADER-->
<div class="pure-g content">
<!--BEGIN left-->
{include file="admin/lib/left.tpl"}
<!--END left-->
    <div class="pure-u-1">
        <div class="main">
            <div class="panel panel-default">
                <div class="panel-title">
                    友情链接
                </div>
                <div class="panel-body">
                    <form class="pure-form pure-form-aligned" action="{'admin/addlink'|url}" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="pure-control-group">
                                <label for="name">标　题：</label>
                                <input name="id" type="hidden" value="{if $data}{$data.id}{/if}" />
                                <input name="name" class="pure-input-1-3" type="text" required value="{if $data}{$data.name}{/if}"/>
                            </div>
							<div class="pure-control-group">
                                <label for="name">网址：</label>
                                <input name="url" class="pure-input-1-3" type="text" required value="{if $data}{$data.url}{/if}"/>
                            </div>								
							<div class="pure-control-group">
                                <label for="name">缩略图：</label>
                                <input name="picurl" class="pure-input-1-3" type="file" />
                            </div>																			
                            <div class="pure-controls">
                                <input class="pure-button pure-button-primary" type="submit" name="submit" value="提交" />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--BEGIN FOOT-->
{include file="admin/lib/copyright.tpl"}
<script type="text/javascript" src="/static/admin/js/edit/ueditor.config.js"></script>

<script src="/static/admin/js/edit/ueditor.all.min.js"></script>
    <script type="text/javascript">
        var ue = UE.getEditor('container');
    </script>
<!--END FOOT-->
</body>
</html>