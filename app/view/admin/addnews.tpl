<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>文章详细</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/static/admin/css/pure-nr-min.css">
<link rel="stylesheet" href="/static/admin/css/fontAwesome/font-awesome.css">
<link rel="stylesheet" href="/static/admin/css/main.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">   

</head>
<body>
<!--BEGIN HEADER-->
{include file="admin/lib/header.tpl"}
<!--END HEADER-->
<div class="pure-g content">
<!--BEGIN left-->
{include file="admin/lib/left.tpl"}
<!--END left-->
    <div class="pure-u-1">
        <div class="main">
            <div class="panel panel-default">
                <div class="panel-title">
                    文章详细
                </div>
                <div class="panel-body">
                    <form class="pure-form pure-form-aligned" action="{'admin/addnews'|url}" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="pure-control-group">
                                <label for="name">标　题：</label>
                                <input name="id" type="hidden" value="{if $data}{$data.id}{/if}" />
                                <input name="name" class="pure-input-1-3" type="text" required value="{if $data}{$data.name}{/if}"/>
                            </div>
                            <div class="pure-control-group">
                                <label >所属分类：</label>
                                        <select name="category" class="pure-input-1-3" >
										{foreach from=$category.data item=a}
											
											<option value='{$a.id}' {if $data}{if $data.category eq $a.id}selected="selected"{/if}{/if}>{$a.name}</option>
										{/foreach}
										</select>
                            </div>
							<div class="pure-control-group">
                                <label for="name">缩略图：</label>
                                <input name="picurl" class="pure-input-1-3" type="file" />
                            </div>								
							<div class="pure-control-group">
                                <label for="name">关键词：</label>
                                <input name="keyword" class="pure-input-1-3" type="text" value="{if $data}{$data.keyword}{/if}"/>
                            </div>						
							<div class="pure-control-group">
                                <label for="desc">描述：</label>
                                <textarea name="desc" class="pure-input-1-3" type="text" >{if $data}{$data.desc}{/if}</textarea>
                            </div>	
							<div class="pure-control-group">
                                <label for="is_use">是否可用：</label>
								
								
                                <input name="is_use" type="radio" value="1" {if $data}{if $data.is_use neq 0}checked="checked"{/if}{else}checked="checked"{/if} />是　
								
                                <input name="is_use" type="radio" value="0" {if $data}{if $data.is_use eq 0}checked="checked"{/if}{/if}　/>否
                            </div>								
                            <div class="pure-control-group">
                                    <script id="container" name="content" class="pure-input-1-3" type="text/plain">
										{if $data}{$data.content}{else}请输入文章内容{/if}
									</script>
                            </div>							
                            <div class="pure-controls">
                                <input class="pure-button pure-button-primary" type="submit" name="submit" value="提交" />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--BEGIN FOOT-->
{include file="admin/lib/copyright.tpl"}
<script type="text/javascript" src="/static/admin/js/edit/ueditor.config.js"></script>

<script src="/static/admin/js/edit/ueditor.all.min.js"></script>
    <script type="text/javascript">
        var ue = UE.getEditor('container');
    </script>
<!--END FOOT-->
</body>
</html>