<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>文章分类</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/static/admin/css/pure-nr-min.css">
<link rel="stylesheet" href="/static/admin/css/fontAwesome/font-awesome.css">
<link rel="stylesheet" href="/static/admin/css/main.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">   
<link rel="stylesheet" href="/static/admin/js/icheck/skins/minimal/blue.css">   
</head>
<body>
<!--BEGIN HEADER-->
{include file="admin/lib/header.tpl"}
<!--END HEADER-->
<div class="pure-g content">
<!--BEGIN left-->
{include file="admin/lib/left.tpl"}
<!--END left-->
    <div class="pure-u-1">
        <div class="main">
            <div class="panel panel-default">
                <div class="panel-title">
                    编辑管理员
                </div>
                <div class="panel-body">
                    <form class="pure-form pure-form-aligned" action="{'admin/addadmin'|url}" method="post">
                        <fieldset>
                            <div class="pure-control-group">
                                <label for="name">管理员姓名：</label>
                                <input name="id" type="hidden" value="{if $data}{$data.id}{/if}" />
                                <input name="username" class="pure-input-1-3" type="text" required value="{if $data}{$data.username}{/if}"/>
                            </div>
							<div class="pure-control-group">
								<label for="name">管理员密码：</label>
								{if $data}
                                <input name="password" class="pure-input-1-3" type="password"  value=""/><span>不修改密码请留空</span>
								{else}
                                <input name="password" class="pure-input-1-3" type="password" required  value=""/>
								{/if}
                            </div>
                            <div class="pure-controls">
                                <input class="pure-button pure-button-primary" type="submit" name="submit" value="提交" />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--BEGIN FOOT-->
{include file="admin/lib/copyright.tpl"}
<!--END FOOT-->
</body>
</html>