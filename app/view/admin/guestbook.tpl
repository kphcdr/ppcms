<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>留言</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/static/admin/css/pure-nr-min.css">
<link rel="stylesheet" href="/static/admin/css/fontAwesome/font-awesome.css">
<link rel="stylesheet" href="/static/admin/css/main.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">      
</head>
<body>
<!--BEGIN HEADER-->
{include file="admin/lib/header.tpl"}
<!--END HEADER-->
<div class="pure-g content">
<!--BEGIN left-->
{include file="admin/lib/left.tpl"}
<!--END left-->
    <div class="pure-u-1">
        <div class="main">
            <div class="panel panel-default" >
                <div class="panel-title">
                    留言
                </div>
				<div class="panel-body">
<table class="pure-table" width="100%" >
    <thead>
        <tr>
            <th width="5%">ID</th>
            <th width="25%">留言人</th>
            <th width="35%">留言内容</th>
            <th width="20%">留言时间</th>
			
            <th width="15%">操作</th>
        </tr>
    </thead>
    <tbody>
	{foreach from=$ca item=a}
	    <tr>
            <td>{$a.id}</td>
            <td>{$a.username}</td>
            <td>{$a.content}</td>
            <td>{$a.createtime|date_format:"%Y-%m-%d %H:%M:%S"}</td>
			<td><a class="pure-button pure-button-xsmall" href="{'admin/delguestbook/id/'|url}{$a.id}"><i class="icon-trash"></i></a></td>        </tr>
	{/foreach}
    </tbody>
</table>

				<ul class="pure-paginator pure-menu">
{$page}
				</ul>

				</div>
			</div>		
        </div>
    </div>
</div>
<!--BEGIN FOOT-->
{include file="admin/lib/copyright.tpl"}
<!--END FOOT-->
</body>
</html>