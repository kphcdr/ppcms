<?php
if ( ! defined('PPPHP')) exit('非法入口');
class news extends model
{
	public  $table = 'news';
	public function __construct()
	{
		parent::__construct();
	}
	public function getcategorylist($limit = 0)
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "orderby DESC",);
		}
		else 
		{
			$arr = array('ORDER' => 'orderby DESC');
		}
		$data = $this->select('category','*',$arr);
		return $data;
	} 
	public function getcategory($id)
	{
		return $this->get("category",'*',array("id"=>$id));
	}
	public function getnewslist($limit=array(0,10),$cid = 0)
	{
		if($cid)
		{
			$where = "`n`.`category` = $cid AND `n`.`is_use` = 1";
		}
		else
		{
			$where = '`n`.`is_use` = 1 ';
		}
		$sql = "SELECT c.name as cn,n.name,n.id,n.desc FROM news as n LEFT JOIN category as c ON n.category = c.id WHERE $where ORDER BY n.id desc LIMIT $limit";
		$data['data'] = $this->query($sql)->fetchAll();
		if($cid)
		{
			$data['count'] = $this->count('news',NULL,NULL,array('`n`.`is_use`'=>1,'AND'=>array('`n`.`category`'=>$cid)));		
		}
		else
		{
 			$data['count'] = $this->count('news',NULL,NULL,array(array('`n`.`is_use`'=>1)));		
		}
		return $data;
	}
	public function getnews($id)
	{
		return $this->get("news",'*',array('AND'=>array("id"=>$id,"is_use"=>'1')));
	}
	public function getlinkslist()
	{
		return $this->select('link','*',array('is_use'=>1));
	}
}