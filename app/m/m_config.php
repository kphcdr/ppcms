<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_config extends model
{
	public  $table = 'config';
	public function __construct()
	{
		parent::__construct();
	}
	public function getconfiglist()
	{
		$return = $this->select($this->table,'*');
		$data = array();
		foreach($return as $a)
		{
			$data[$a['name']] = $a['value'];
		}
		return $data;
	}
	public function setconfig($data)
	{
		//print_r($data);
		foreach($data as $key=>$a)
		{
			$this->update($this->table, array('value'=>$a),array('name'=>$key));
		}

	}
	public function getconfig($name)
	{
		return $this->get($this->table,'*',array('name'=>$name));
	}
	public function getlinklist()
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "id DESC",);
		}
		else 
		{
			$arr = array('ORDER' => "id DESC",);
		}
		$data['data'] = $this->select('link','*',$arr);
		$data['count'] = $this->count('link');
		return $data;
	}
	public function getlink($id)
	{
		return $this->get('link','*',array());
	}
	public function setlink($id,$data)
	{
		return $this->update('link',$data,array('id'=>$id));
	}
	public function addlink($data)
	{
		return $this->insert('link', $data);
	}
	public function dellink($id)
	{
		return $this->delete('link', array('id'=>$id));
	}
}