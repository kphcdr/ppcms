<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_member extends model
{
	public  $table = 'member';
	public function __construct()
	{
		parent::__construct();
	}
	public function lists($limit = 0)
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "id DESC",);
		}
		else 
		{
			$arr = array('ORDER' => "id DESC",);
		}
		$data['data'] = $this->select($this->table,'*',$arr);
		$data['count'] = $this->count($this->table);
		return $data;
	}
	public function delmember($id)
	{
		return $this->delete($this->table, array('id'=>$id));
	}
}