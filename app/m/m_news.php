<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_news extends model
{
	public  $table = 'news';
	public function __construct()
	{
		parent::__construct();
	}
	public function getcategorylist($limit = 0)
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "id DESC",);
		}
		else 
		{
			$arr = array('ORDER' => "id DESC",);
		}
		$data['data'] = $this->select('category','*',$arr);
		$data['count'] = $this->count('category');
		return $data;
	}
	public function getcategory($id)
	{
		return $this->get("category",'*',array("id"=>$id));
	}
	public function addcategory($data)
	{
		return $this->insert('category',$data);
	}
	public function editcategory($id,$data)
	{
		return $this->update('category',$data,array('id'=>$id));
	}
	public function delcategory($id)
	{
		return $this->delete('category', array('id'=>$id));
	}
	//判断分类下是否有文章
	public function category_have_news($id)
	{
		return $this->count('news',array('category'=>$id));
	}
	public function getnewslist($limit=array(0,10))
	{
		$sql = "SELECT c.name as cn,n.* FROM news as n LEFT JOIN category as c ON n.category = c.id ORDER BY n.id desc LIMIT $limit";
//		$data['data'] = $this->select('news',array('[>]category'=>array('category'=>'id')),'category.name,news.*',array('LIMIT'=>$limit, 'ORDER' => "id DESC",));
		$data['data'] = $this->query($sql);
		$data['count'] = $this->count('news');
		return $data;
	}
	public function getnews($id)
	{
		return $this->get("news",'*',array("id"=>$id));
	}
	public function addnews($data)
	{
		return $this->insert('news',$data);
	}
	public function editnews($id,$data)
	{
		return $this->update('news',$data,array('id'=>$id));
	}
	public function delnews($id)
	{
		return $this->delete('news', array('id'=>$id));
	}
}