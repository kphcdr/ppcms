<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_article extends model
{
	public  $table = 'article';
	public function __construct()
	{
		parent::__construct();
	}
	public function lists($limit = 0)
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "id DESC",);
		}
		else 
		{
			$arr = array('ORDER' => "id DESC",);
		}
		$data['data'] = $this->select($this->table,'*',$arr);
		$data['count'] = $this->count($this->table);
		return $data;
	}
	
	public function getnews($id)
	{
		return $this->get($this->table,'*',array("id"=>$id));
	}
	public function addnews($data)
	{
		return $this->insert($this->table,$data);
	}
	public function editnews($id,$data)
	{
		return $this->update($this->table,$data,array('id'=>$id));
	}
	public function delnews($id)
	{
		return $this->delete($this->table, array('id'=>$id));
	}
}