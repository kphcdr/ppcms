<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_admin extends model
{
	public  $table = 'admin';
	public function __construct()
	{
		parent::__construct();
	}
	public function lists()
	{
		return $this->select('test','*',array('id[>=]'=>'1','LIKE'=>array('name'=>'1234'),'LIMIT'=>array(0, 100)));
	}
	public function add($data)
	{
		return $this->insert('tucao_m', $data);
	}
	public function del()
	{
		return $this->delete('tucao_m', array('name'=>'kph'));
	}
	public function set($id,$data)
	{
		return $this->update('news',$data,array('id'=>$id));
	}
	public function get_username_password($arr)
	{
		$return = $this->get($this->table, array('id') ,array('AND'=>$arr,));	
		return $return;
	}
}