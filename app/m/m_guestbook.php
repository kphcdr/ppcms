<?php
if ( ! defined('PPPHP')) exit('非法入口');
class m_guestbook extends model
{
	public  $table = 'guestbook';
	public function __construct()
	{
		parent::__construct();
	}
	public function lists($limit = 0)
	{
		if($limit)
		{
			$arr = array('LIMIT'=>$limit, 'ORDER' => "id DESC",);
		}
		else 
		{
			$arr = array('ORDER' => "id DESC",);
		}
		$data['data'] = $this->select($this->table,'*',$arr);
		$data['count'] = $this->count($this->table);
		return $data;
	}
	public function delguestbook($id)
	{
		return $this->delete($this->table, array('id'=>$id));
	}
}