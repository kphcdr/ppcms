<?php
if ( ! defined('PPPHP')) exit('非法入口');

class admin extends ppphp 
{
	private $nopower = array('login');#不需要权限的方法
	public $id;#管理员ID
	private $c;#cache缓存类
	public function __construct()
	{
		parent::__construct();
		if(!in_array($this->__a, $this->nopower))
		{
			$this->__checkpower();
		}
		$this->c = $this->b('ocs_cache');
		$this->c->add_server();
	}
	public function index()
	{
		jump('/admin/lists');
	}
	public function delcache()
	{
		$this->c->clear();
		msg('更新成功');
	}
	public function login()
	{
		if(is_post())
		{
			$data['username'] = $this->post('username');
			$data['password'] = md5($this->post('password').'kphcdr');
			$adminM = $this->m('m_admin');
			
			$result = $adminM->get_username_password($data);
			if($result)
			{
			$session = $this->b('session');
			$session->set('id',$result['id']);
			$adminM->set($result['id'],array('lasttime'=>time()));
			jump('/admin/index');
			}
			else
			{
				msg('账号或密码错误');
			}
		}
		else
		{
			$this->display('login');
		}
	}
	public function out()
	{
		$session = $this->b('session');
		$session->dropall();
		jump('/admin/login');
	}
	public function lists()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit = ($page - 1)*$pagenum.','.$pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$news = $this->m('m_news');

		$ca = $news->getnewslist($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = $pagenum;
		$pageB->url = url('admin/lists/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();

		$this->display('lists',$data);
	}	
	public function addnews()
	{
		$news = $this->m('m_news');
		if(is_post())
		{
			//文件上传
			if($_FILES['picurl']['name'])
			{
			    $upload = $this->b('upload',array('uploadPath'=>'./upload/news'));
			    $upload->fileUpload($_FILES['picurl']);
			    $uploadstate = $upload->getStatus();
			    if($uploadstate['error'] == 0)
			    {
			    	$data['picurl'] = $upload->getfilename();
			    }
			    else 
			    {
			    	exit($uploadstate['message']);
			    }			
			}
			$data['name'] = $this->post('name');
			$data['category'] = $this->post('category');
			$data['content'] = $this->post('content');
			$data['keyword'] = $this->post('keyword');
			$data['desc'] = $this->post('desc');
			$data['createtime'] = time();
			$data['is_use'] = $this->post('is_use');
			$id = $this->post('id');
			//$id不为0就是插入
			if($id)
			{
				$result = $news->editnews($id,$data);
				if($result)
				{
					msg('修改成功');
				}
				else
				{
					msg('修改失败');
				}
			}
			else
			{
				$result = $news->addnews($data);
				if($result)
				{
					msg('添加成功');
				}
				else
				{
					msg('添加失败');
				}				
			}
		}
		else
		{
			$id = $this->get('id');
			if($id)
			{
				$data['data'] = $news->getnews($id);
			}
			else 
			{
				$data['data'] = '';
			}
			$data['category'] = $news->getcategorylist();
			$this->display('addnews',$data);
		}		
	}
	public function delnews()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$news = $this->m('m_news');
			$result = $news->delnews($id);
			if($result)
			{
				msg('删除成功');
			}
			else
			{
				msg('删除失败');
			}
		}		
	}
	//文章分类
	public function category()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit[0] = ($page - 1)*$pagenum;
			$limit[1] = $pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$category = $this->m('m_news');
		
		$ca = $category->getcategorylist($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = 10;
		$pageB->url = url('admin/category/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();
		$this->display('category',$data);
	}
	public function delcategory()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$category = $this->m('m_news');
			//判断分类下是否有文章
			if($category->category_have_news($id))
			{
				msg('该分类下还有文章，请先删除文章');
			}
			else 
			{
				$result = $category->delcategory($id);
				if($result)
				{
					msg('删除成功');
				}
				else
				{
					msg('删除失败');
				}
			}
		}
	}
	public function addcategory()
	{
		$category = $this->m('m_news');
		if(is_post())
		{
			$data['name'] = $this->post('name');
			$id = $this->post('id');
			//$id不为0就是插入
			if($id)
			{
				$result = $category->editcategory($id,$data);
				if($result)
				{
					msg('修改成功');
				}
				else
				{
					msg('修改失败');
				}
			}
			else
			{
				$result = $category->addcategory($data);
				if($result)
				{
					msg('添加成功');
				}
				else
				{
					msg('添加失败');
				}				
			}
		}
		else
		{
			$id = $this->get('id');
			if($id)
			{
				$data['data'] = $category->getcategory($id);
			}
			else 
			{
				$data['data'] = '';
			}
			$this->display('addcategory',$data);
		}
	}
	public function config()
	{
		$data = array();
		$config = $this->m('m_config');
		
		if(is_post())
		{
			$data['name'] = $this->post('name');
			$data['keyword'] = $this->post('keyword');
			$data['desc'] = $this->post('desc');
			$data['icp'] = $this->post('icp');
			$data['tel'] = $this->post('tel');
			$config->setconfig($data);
			msg('修改成功');
		}
		else 
		{
			$data['data'] = $config->getconfiglist();
			$this->display('config',$data);
		}
	}
	public function link()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit = ($page - 1)*$pagenum.','.$pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$link = $this->m('m_config');

		$ca = $link->getlinklist($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = 10;
		$pageB->url = url('admin/link/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();

		$this->display('link',$data);		
	}
	//文章单页
	public function article()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit[0] = ($page - 1)*$pagenum;
			$limit[1] = $pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$article = $this->m('m_article');
		
		$ca = $article->lists($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = 10;
		$pageB->url = url('admin/article/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();
		$this->display('article',$data);
	}
	//新建单页
	public function addarticle()
	{
		$article = $this->m('m_article');
		if(is_post())
		{
			//文件上传
			if($_FILES['picurl']['name'])
			{
			    $upload = $this->b('upload',array('uploadPath'=>'./upload/article'));
			    $upload->fileUpload($_FILES['picurl']);
			    $uploadstate = $upload->getStatus();
			    if($uploadstate['error'] == 0)
			    {
			    	$data['picurl'] = $upload->getfilename();
			    }
			    else 
			    {
			    	exit($uploadstate['message']);
			    }			
			}
			$data['name'] = $this->post('name');
			$data['content'] = $this->post('content');
			$data['desc'] = $this->post('desc');
			$data['createtime'] = time();
			$id = $this->post('id');
			//$id不为0就是插入
			if($id)
			{
				$result = $article->editnews($id,$data);
				if($result)
				{
					msg('修改成功');
				}
				else
				{
					msg('修改失败');
				}
			}
			else
			{
				$result = $article->addnews($data);
				if($result)
				{
					msg('添加成功');
				}
				else
				{
					msg('添加失败');
				}				
			}
		}
		else
		{
			$id = $this->get('id');
			if($id)
			{
				$data['data'] = $article->getnews($id);
			}
			else 
			{
				$data['data'] = '';
			}
			$this->display('addarticle',$data);
		}		
	}
	//删除单页
	public function delarticle()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$news = $this->m('m_article');
			$result = $news->delnews($id);
			if($result)
			{
				msg('删除成功');
			}
			else
			{
				msg('删除失败');
			}
		}
	}
	public function addlink()
	{
		$news = $this->m('m_config');
		if(is_post())
		{
			//文件上传
			if($_FILES['picurl']['name'])
			{

			    $upload = $this->b('upload',array('uploadPath'=>'./upload/link'));
			    $upload->fileUpload($_FILES['picurl']);
			    $uploadstate = $upload->getStatus();
			    if($uploadstate['error'] == 0)
			    {
			    	$data['picurl'] = $upload->getfilename();
			    }
			    else 
			    {
			    	exit($uploadstate['message']);
			    }			
			}
			$data['name'] = $this->post('name');
			$data['url'] = $this->post('url');
			$data['createtime'] = time();
			$id = $this->post('id');
			//$id不为0就是插入
			if($id)
			{
				$result = $news->setlink($id,$data);
				if($result !== NULL)
				{
					msg('修改成功');
				}
				else
				{
					
					msg('修改失败');
				}
			}
			else
			{
				$result = $news->addlink($data);
				if($result)
				{
					msg('添加成功');
				}
				else
				{
					msg('添加失败');
				}				
			}
		}
		else
		{
			$id = $this->get('id');
			if($id)
			{
				$data['data'] = $news->getlink($id);
			}
			else 
			{
				$data['data'] = '';
			}
			$this->display('addlink',$data);
		}
	}
	public function dellink()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$news = $this->m('m_config');
			$result = $news->dellink($id);
			if($result)
			{
				msg('删除成功');
			}
			else
			{
				msg('删除失败');
			}
		}	
	}
	//会员
	public function member()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit[0] = ($page - 1)*$pagenum;
			$limit[1] = $pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$member = $this->m('m_member');
		
		$ca = $member->lists($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = 10;
		$pageB->url = url('admin/member/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();
		$this->display('member',$data);
	}
	public function delmember()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$news = $this->m('m_member');
			$result = $news->delmember($id);
			if($result)
			{
				msg('删除成功');
			}
			else
			{
				msg('删除失败');
			}
		}	
	}
	//会员
	public function guestbook()
	{
		$page = $this->get('page','int');
		$pagenum = 10;
		if($page)
		{
			$limit[0] = ($page - 1)*$pagenum;
			$limit[1] = $pagenum;
		}
		else 
		{
			$page = 1;
			$limit = $pagenum;
		}
		$guestbook = $this->m('m_guestbook');
		
		$ca = $guestbook->lists($limit);
		$t = $this->b('T');
		$pageB = $this->b('page');
		$pageB->page_now = $page;
		$pageB->total = $ca['count'];
		$pageB->page_num = 10;
		$pageB->url = url('admin/guestbook/page/');
		$data['ca'] = $ca['data'];
		$data['page'] = $pageB->get_page();
		$this->display('guestbook',$data);
	}
	public function delguestbook()
	{
		$id = $this->get('id','int');
		if($id)
		{
			$news = $this->m('m_guestbook');
			$result = $news->delguestbook($id);
			if($result)
			{
				msg('删除成功');
			}
			else
			{
				msg('删除失败');
			}
		}	
	}
	private function __checkpower()
	{
		//检查session
		$session = $this->b('session');
		$this->id = $session->get('id');
		if(empty($this->id))
		{
			jump(url('/admin/login'));
		}
		else
		{
			$session->set('id',$this->id);
		}
	}
}