<?php
if ( ! defined('PPPHP')) exit('非法入口');

class index extends ppphp 
{
	private $news;
	private $c;#cache文件缓存类
	public function __construct()
	{
		parent::__construct();
		$this->config = $this->m('m_config');
		$this->news = $this->m('news');
		$this->c = $this->b('ocs_cache');
		$this->c->add_server();
		
	}
	public function index()
	{
		//全局配置
		$data['config'] = $this->header();
		//文章分类
		$data['category'] = $this->category();
		//文章列表
		$data['news'] = $this->getnews();
		//友情链接
		$data['link'] =$this->links();
		$data['id'] = 0;
		$this->display('index',$data);
	}
	public function c()
	{
		$id = $this->get('id','int');
		if(!$id)
		{
			msg('缺少参数');
		}
		$data['id'] = $id;
		//全局配置
		$data['config'] = $this->header();
		//文章分类
		$data['category'] = $this->category();
		//友情链接
		$data['link'] =$this->links();
		
		$data['c'] = $this->news->getcategory($id);
		$data['lists'] = $this->getnews($id);
		$data['news'] = $this->getnews();

			$data['config']['name'] = $data['c']['name'].'|'.$data['config']['name'];
			$data['config']['keyword'] = $data['c']['name'].'|'.$data['config']['keyword'];
		$this->display('c',$data);
	}
    public function news()
    {
	
			$id = $this->get('id','int');
			if(!$id)
			{
				msg('缺少参数');
			}
		if(!$data = $this->c->get('news'.$id))
		{				
			//全局配置
			$data['config'] = $this->header();
			$data['config']['name'] = ''.$data['config']['name'];
			//文章分类
			$data['category'] = $this->category();
			
			//友情链接
			$data['link'] =$this->links();
			
			$data['lists'] = $this->news->getnews($id);
			$data['id'] = $data['lists']['category'];
			//文章列表
			$data['news'] = $this->getnews();
		
			$data['config']['name'] = $data['lists']['name'].'|'.$data['config']['name'];
			$data['config']['keyword'] = $data['lists']['keyword']?$data['lists']['keyword']:$data['config']['keyword'];
			$data['config']['desc'] = $data['lists']['desc']?$data['lists']['desc']:$data['config']['desc'];
			$this->c->set('news'.$id,$data);
		}	
			$this->display('news',$data);
    }
    private function header()
    {
		if(!$return = $this->c->get('header'))
		{
			$return = $this->config->getconfiglist();
			$this->c->set('header',$return);			
		}
		return $return;
    }
    private function category()
    {
		if(!$return = $this->c->get('category'))
		{
			$return = $this->news->getcategorylist();
			$this->c->set('category',$return);			
		}
		return $return;
    }
    private function getnews($id=0)
    {
		if(!$data = $this->c->get('getnews'.$id))
		{	
			$page = $this->get('page','int');
			$pagenum = 20;
			if($page)
			{
				$limit = ($page - 1)*$pagenum.','.$pagenum;
			}
			else 
			{
				$page = 1;
				$limit = $pagenum;
			}

			$ca = $this->news->getnewslist($limit,$id);
			$pageB = $this->b('page');
			$pageB->page_now = $page;
			$pageB->total = $ca['count'];
			$pageB->page_num = $pagenum;
			$pageB->url = url('index/index/page/');
			$data['data'] = $ca['data'];
			if($ca['count']<$pagenum)
			{
				$data['page'] = '';
			}
			else
			{
				$data['page'] = $pageB->get_page();
			}
			$this->c->set('getnews'.$id,$data);			
		}
			return $data;	
    }
	private function links()
	{
		if(!$return = $this->c->get('links'))
		{
			$return = $this->news->getlinkslist();
			$this->c->set('links',$return);			
		}
		return $return;
	}
}